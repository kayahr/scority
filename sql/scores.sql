DROP TABLE IF EXISTS scores;
CREATE TABLE scores
(
    id SERIAL NOT NULL,
    game_id INT4 NULL,
    player VARCHAR(64) NULL,
    points INT4 NULL,
    level INT4 NULL,
    started TIMESTAMP NULL,
    submitted TIMESTAMP NOT NULL,
    ip VARCHAR(40) NULL,
    agent VARCHAR(255) NULL,
    accepted BOOLEAN NOT NULL,
    reject_reason VARCHAR(255) NULL,
    data TEXT NOT NULL,
    checksum INT8 NOT NULL,
    PRIMARY KEY(id)
);
ALTER TABLE scores
    ADD CONSTRAINT scores_game_id
    FOREIGN KEY(game_id)
    REFERENCES games(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

CREATE INDEX scores_game_id ON scores (game_id);
CREATE UNIQUE INDEX scores_uniqueness ON scores (game_id, points, started, checksum);
CREATE INDEX scores_points ON scores (points DESC);
CREATE INDEX scores_sort_order ON scores (points DESC, submitted DESC);
CREATE INDEX scores_accepted ON scores (accepted DESC);
