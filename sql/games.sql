DROP TABLE IF EXISTS games;
CREATE TABLE games
(
    id SERIAL NOT NULL,
    name VARCHAR(32) UNIQUE NOT NULL,
    validator VARCHAR(64) UNIQUE NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO games (name, validator) VALUES ('destroids', 'DestroidsValidator');
