<?php
/*
 * Copyright (C) 2011 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */

namespace Scority;

use Exception;
use PDO;

/**
 * Database access.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
class DB
{
    /** The database backend. */
    private static $db;
    
    /**
     * Returns the singleton instance of the database backend.
     * 
     * @return PDO
     *            The database backend.
     */    
    private static function getDb()
    {
        if (!self::$db)
        {
            self::$db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$db;
    }
    
    /**
     * Performs a query.
     * 
     * @param string $sql
     *            The SQL statement.
     * @param array $params
     *            Hash map with parameters.
     * @return array
     *            Array of hash maps with result data. Never null.
     * @throws Exception
     *            When query failed.
     */
    public static function query($sql, $params = array())
    {
        $stmt = self::getDb()->prepare($sql);
        if (!$stmt->execute($params))
        {
            $error = $stmt->errorInfo();
            throw new Exception($error[2]);
        }
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $data;
    }
    
    /**
     * Performs a query for a single result. Only the first row is returned.
     * If the result was empty then NULL is returned.
     * 
     * @param string $sql
     *            The SQL statement.
     * @param array $params
     *            Hash map with parameters.
     * @return array
     *            Hash map with result data of first row or null if no row
     *            was found.
     * @throws Exception
     *            When query failed.
     */
    public static function querySingle($sql, $params = array())
    {
        $result = self::query($sql, $params);
        if (count($result) == 0) return NULL;
        return $result[0];
    }
    
    /**
     * Executes a SQL command.
     * 
     * @param string $sql
     *            The SQL statement.
     * @param array $params
     *            Hash map with parameters.
     * @throws Exception
     *            When execution failed.
     */
    public static function exec($sql, $params = array())
    {
        $stmt = self::getDb()->prepare($sql);
        if (!$stmt->execute($params))
        {
            $error = $stmt->errorInfo();
            throw new Exception($error[2]);
        }
        $stmt->closeCursor();
    }

    /**
     * Starts a new transaction.
     */
    public static function beginTransaction()
    {
        self::getDb()->beginTransaction();
    }

    /**
     * Commits the current transaction.
     */    
    public static function commit()
    {
        self::getDb()->commit();
    }
    
    /**
     * Performs a rollback of the current transaction.
     */
    public static function rollBack()
    {
        self::getDb()->rollBack();
    }
}
