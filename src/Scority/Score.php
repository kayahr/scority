<?php
/*
 * Copyright (C) 2010 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */

namespace Scority;

/**
 * Score object.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
class Score
{
    /** The game */
    private $game;
    
    /** The rejection reason */
    private $rejectReason;
    
    /** If score was accepted */
    private $accepted = false;
    
    /** The points */
    private $points;
    
    /** The level */
    private $level;
    
    /** The player */
    private $player;
    
    /** The game journal */
    private $journal;
    
    /** The timestamp when the game was started */
    private $started;
    
    /**
     * Constructs a new score.
     * 
     * @param string $game
     *            The game
     * @param string $data
     *            The score data
     */
    public function __construct($game, $data)
    {
        $this->game = $game;
        $this->data = $data;
        $this->parse();
    }
    
    /**
     * Parses score data
     * 
     * @param string $data
     *            The data to parse
     */
    private function parse()
    {
        $parts = explode(":", $this->data, 2);
        if (count($parts) != 2) return $this->reject("No size/data split");
        $size = $parts[0];
        $data = $parts[1];
        if ($size != strlen($data)) return $this->reject("Data size mismatch");
        
        $parts = explode("/", $data, 2);
        if (count($parts) != 2) return $this->reject("No points/data split");
        $points = $parts[0];
        $data = $parts[1];
        $points = intval($points, 36);
        if ($points <= 0) return $this->reject("Invalid points");
        $this->points = $points;
        
        $parts = explode("?", $data, 2);
        if (count($parts) != 2) return $this->reject("No level/data split");
        $level = $parts[0];
        $data = $parts[1];
        $level = intval($level, 36);
        if ($level <= 0) return $this->reject("Invalid level");
        $this->level = $level;
        
        $parts = explode("@", $data, 2);
        if (count($parts) != 2) return $this->reject("No journal/player split");
        $journal = $parts[0];
        $player = $parts[1];
        if (!$player) return $this->reject("No player set");
        if (!$journal) return $this->reject("No journal set");
        $this->player = $player;
        $this->journal = $journal;
        
        $this->validate();
    }
    
    /**
     * Validates the journal.
     */
    private function validate()
    {
        $validatorClassName = DAO::getValidatorClassName($this->game);
        if (!$validatorClassName) return $this->reject("No validator found");
        
        try
        {        
            $validatorClassName = "\\Scority\\Validators\\$validatorClassName";
            $validator = new $validatorClassName();
            if (!$validator->validate($this)) return;            
        }
        catch (Exception $exception)
        {
            return $this->reject("Validator error: " . $exception->getMessage());
        }
        
        $this->accept();
    }
    
    /**
     * Rejects the score with the specified reason.
     * 
     * @param string $reason
     *            The reject reason
     */
    public function reject($reason)
    {
        $this->rejectReason = $reason;
        $this->accepted = false;
        return false;
    }
    
    /**
     * Accepts the score.
     */
    private function accept()
    {
        $this->accepted = true;
    }
    
    /**
     * Checks if score was rejected.
     * 
     * @return boolean 
     *            True if score was rejected, false if not
     */
    public function isRejected()
    {
       return !$this->accepted;
    }
    
    /**
     * Returns the reject reason.
     * 
     * @return string 
     *            The reject reason
     */
    public function getRejectReason()
    {
        return $this->rejectReason;
    }

    /**
     * Returns the game.
     * 
     * @return string
     *            The game.
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Returns the player.
     *
     * @return string
     *             The player.
     */    
    public function getPlayer()
    {
        return $this->player;
    }
    
    /**
     * Returns the journal.
     *
     * @return string
     *            The journal.
     */
    public function getJournal()
    {
        return $this->journal;
    }
    
    /**
     * Returns the points.
     *
     * @return number
     *            The points.
     */
    public function getPoints()
    {
        return $this->points;
    }
    
    /**
     * Returns the level.
     *
     * @return number
     *            The level.
     */
    public function getLevel()
    {
        return $this->level;
    }
    
    /**
     * Sets the timestamp when the game started.
     *
     * @param number $started
     *            The timestamp to set.
     */
    public function setStarted($started)
    {
        $this->started = $started;
    }
    
    /**
     * Returns the timestamp when the game started.
     *
     * @return number
     *             The timestamp.
     */
    public function getStarted()
    {
        return $this->started;
    }
    
    /**
     * Returns the score data.
     *
     * @return string
     *             The score data.
     */
    public function getData()
    {
        return $this->data;
    }
}
