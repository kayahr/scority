<?php
/*
 * Copyright (C) 2010 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */

namespace Scority;

/**
 * Interface for score validators.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
abstract class ScoreValidator
{
    /** The score to validate */
    private $score;
    
    /**
     * Validates a score.
     * 
     * @param Score $score
     *            The score to validate
     * @return boolean True if score is valid, false if not
     */
    public function validate(Score $score)
    {
        $this->score = $score;
        $data = explode("I", $score->getJournal());
        if (count($data) < 2)
            return $this->reject("No timestamp/data split");
        $timestamp = intval(array_shift($data), 36);
        $score->setStarted($timestamp);
        if (!$this->init($score->getPoints(), $score->getLevel(), $timestamp)) return false;
        while ($data)
        {
            // Calculate next timestamp    
            $delta = intval(array_shift($data), 36);    
            $timestamp += $delta;
            
            // get action
            $action = intval(array_shift($data), 36);
            
            if (!$this->action($action, $timestamp, $delta)) return false;
        }
        if (!$this->done()) return false;
        
        return true;
    }
    
    /**
     * Rejects the score.
     * 
     * @param string $reason
     *            The rejection reason
     */
    protected function reject($reason)
    {
        return $this->score->reject($reason);
    }
    
    /**
     * Initializes the validator.
     * 
     * @param number $points
     *            The unvalidated points
     * @param number $level
     *            The unvalidated level
     * @param number $timestamp
     *            The start timestamp of the journal
     */
    protected abstract function init($points, $level, $timestamp);
    
    /**
     * Processes one action in the journal.
     * 
     * @param number $action
     *            The action
     * @param number $timestamp
     *            The timestamp
     * @param number $diff
     *            The time difference to the previous step
     */
    protected abstract function action($action, $timestamp, $delta);
    
    /**
     * End validation.
     */
    protected abstract function done();
}
