<?php
/*
 * Copyright (C) 2010 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */
 
namespace Scority\Validators;

use Scority\ScoreValidator;

/**
 * Score validator for Destroids.
 *
 * @author Klaus Reimer (k@ailis.de)
 */
class DestroidsValidator extends ScoreValidator
{
    /** The unvalidated points */
    private $points;
    
    /** The timestamp */
    private $timestamp;
    
    /** The current level */
    private $level;
    
    /** The real points */
    private $realPoints;
    
    /** The real levl */
    private $realLevel;
    
    /** If game is finished */
    private $finish;
    
    /**
     * @see ScoreValidator#init(number, number, number)
     */
    public function init($points, $level, $timestamp)
    {
        $this->points = $points;
        $this->level = $level;
        $this->timestamp = $timestamp;
        $this->realPoints = 0;
        $this->realLevel = 1;
        $this->finish = false;
        return true;
    }    
    
    /**
     * @see ScoreValidator#action(number, number, number)
     */
    public function action($action, $timestamp, $delta)
    {
        if ($this->finish) return $this->reject("Action after finish");
        if ($delta >= 24 * 60 * 60) return $this->reject("Time delta too long: $delta");
        
        switch ($action)
        {
            // Eject bonus
            case 1:
                $add = intval($this->realPoints / 1000) * 100;
                $this->realPoints += $add;
                $this->finish = true;
                break;
                
            // Shot UFO
            case 2:
                $add = 100 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Collected Repairkit
            case 3:
                $add = 25 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Collected Energy
            case 4:
                $add = 25 * $this->realLevel;
                $this->realPoints += $add;
                break;
            
            // Shot small asteroid
            case 5:
                $add = 50 * $this->realLevel;
                $this->realPoints += $add;
                break;
    
            // Shot large asteroid
            case 6:
                $add = 20 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Collided with small asteroid
            case 7:
                $add = 50 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Collided with large asteroid
            case 8:
                $add = 20 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Collided with UFO
            case 9:
                $add = 100 * $this->realLevel;
                $this->realPoints += $add;
                break;
                
            // Next level
            case 10:
                $this->realLevel++;
                break;
    
            // Ship destroyed
            case 11:
                $this->finish = true;
                break;
    
            // Collected powerup
            case 12:
                $add = 25 * $this->realLevel;
                $this->realPoints += $add;
                break;
            
            // Don't accept any other action
            default:
                return $this->reject("Unknown action: $action");
                break;
        }
        
        if ($this->realPoints > $this->points)
            return $this->reject("Points overflow: " . $this->realPoints .
                " > " . $this->points);
        
        if ($this->realLevel > $this->level)
            return $this->reject("Level overflow: " . $this->realLevel .
                " > " . $this->level);
        
        return true;       
    }

    /**
     * @see ScoreValidator#done(number)
     */
    public function done()
    {
        if ($this->realPoints != $this->points)
            return $this->reject("Points mismatch: " . $this->realPoints .
                " != " . $this->points);
            
        if ($this->realLevel != $this->level)
            return $this->reject("Level mismatch: " . $this->realLevel .
                " != " . $this->level);
            
        return true;
    }
}
