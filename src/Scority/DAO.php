<?php
/*
 * Copyright (C) 2010 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */
 
namespace Scority;

/**
 * Data access object
 *
 * @author Klaus Reimer (k@ailis.de)
 */
class DAO
{
    /**
     * Returns the highscores for the specified game. Only the top
     * <var>size</var> entries are returned.
     * 
     * @param string $game
     *            The game name
     * @param number $size
     *            The highscore list size
     * @return array
     *            The highscore list
     */
    public static function getHighscores($game, $size)
    {
        return DB::query(
            "SELECT player, level, points " .
            "FROM scores AS h " .
            "INNER JOIN games AS g ON g.id = h.game_id " .
            "WHERE g.name = :game ". 
            "AND accepted " .
            "ORDER BY h.points DESC, h.submitted DESC " .
            "LIMIT :size",
            array(
                "game" => $game,
                "size" => $size
            ));
    }
    
    /**
     * Gets the validator class name for the specified game.
     * 
     * @param string $game
     *            The game name
     * @return string The class name of the validator class
     */
    public static function getValidatorClassName($game)
    {
        $row = DB::querySingle(
            "SELECT validator FROM games WHERE name = :game",
            array(
                "game" => $game,
            ));
        return $row["validator"];
    }
    
    /**
     * Returns the ID of the specified game.
     * 
     * @param string $game
     *            The game name
     * @param number The game id
     */
    public static function getGameId($game)
    {
        $row = DB::querySingle(
            "SELECT id FROM games WHERE name = :game",
            array(
                "game" => $game,
            ));
        return $row["id"];        
    }
    
    /**
     * Check if a score is already in the database.
     * 
     * @param number gameId
     *            The game id
     * @param score
     *            The score object
     * @return True if score is already registered, false if not
     */
    public static function scoreExists($gameId, Score $score)
    {
        $points = $score->getPoints();
        $checksum = crc32($score->getData());
        $started = $score->getStarted();
        
        $row = DB::querySingle(
            "SELECT id FROM scores " .
            "WHERE game_id = :gameId AND points = :points AND started = to_timestamp(:started) AND checksum = :checksum",
            array(
                "gameId" => $gameId,
                "points" => $points,
                "checksum" => $checksum,
                "started" => $started
           ));   
        return !!$row;
    }
    
    /**
     * Returns the rank achieved with the specified points.
     * 
     * @param number gameId
     *            The game id
     * @param number points
     *            The points
     * @return The rank 
     */
    public static function getRank($gameId, $points)
    {
        $row = DB::querySingle(
            "SELECT COUNT(id) AS rank FROM scores " .
            "WHERE game_id = :gameId AND points > :points AND accepted",
            array(
                "gameId" => $gameId,
                "points" => $points
            ));
        return $row["rank"] + 1;
    }
    
    /**
     * Adds a score.
     * 
     * @param Score score
     *            The score to add
     * @return number
     *            The achieved rank
     */
    public static function addScore(Score $score)
    {
        $gameId = self::getGameId($score->getGame());
        $points = $score->getPoints();
        $rank = self::getRank($gameId, $points);        
        
        // Ignore if already present
        if (self::scoreExists($gameId, $score)) return $rank;
        
        $level = $score->getLevel();
        $player = $score->getPlayer();
        $data = $score->getData();
        $checksum = crc32($data);
        $started = $score->getStarted();
        $submitted = time();
        $ip = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : null;
        $agent = isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : null;
        $accepted = !$score->isRejected();
        $rejectReason = $score->getRejectReason();
        
        DB::exec(
            "INSERT INTO scores " .
            "(game_id, player, points, level, data, started, submitted, ip, agent, accepted, reject_reason, checksum) " .
            "VALUES ".
            "(:gameId, :player, :points, :level, :data, to_timestamp(:started), to_timestamp(:submitted), :ip, :agent, :accepted, :rejectReason, :checksum)",
            array(
                "gameId" => $gameId,
                "player" => $player,
                "points" => $points,
                "level" => $level,
                "data" => $data,
                "checksum" => $checksum,
                "started" => $started,
                "submitted" => $submitted,
                "ip" => $ip,
                "agent" => $agent,
                "accepted" => $accepted,
                "rejectReason" => $rejectReason
           ));            
           
        return $rank;
    }
}
