<?php
/*
 * Copyright (C) 2010 Klaus Reimer <k@ailis.de>
 * See LICENSE.txt for licensing information.
 */

require dirname(__FILE__) ."/common.php";

use Scority\DAO;
use Scority\Score;

/**
 * Reads the game from the URL and returns it.
 * 
 * @return string 
 *            The game
 */
function getGame()
{
    $game = array_shift($_SERVER["argv"]);
    if (!$game) fail(400, "No game specified");
    return $game;
}

/**
 * Sends an error response.
 * 
 * @param number $status
 *            The HTTP status code to send back
 * @param string $message
 *            The text message to send back
 */
function fail($status, $message)
{
    header("HTTP/1.0 $status $message");
    echo "$message\n";
    exit($status);
}

// Set arguments if not already done (On command-line this is already done)
if (!isset($_SERVER["argv"]) || !$_SERVER["argv"])
{
    if (isset($_SERVER["PATH_INFO"]))
    {
        $_SERVER["argv"] = explode("/", rtrim($_SERVER["PATH_INFO"], "/"));
        $_SERVER["argv"][0] = $_SERVER["SCRIPT_NAME"];
    }
    else
    {
        $_SERVER["argv"] = array($_SERVER["SCRIPT_NAME"]);
    }
    $_SERVER["argc"] = count($_SERVER["argv"]);
}

// Process command
array_shift($_SERVER["argv"]);
$command = array_shift($_SERVER["argv"]);
if (!$command) fail(400, "No command specified");
switch ($command)
{
    case "top5":
        $highscores = DAO::getHighscores(getGame(), 5);
        header("Content-Type: text/plain");
        echo json_encode($highscores) . "\n";
        break;
        
    case "top25":
        $highscores = DAO::getHighscores(getGame(), 25);
        header("Content-Type: text/plain");
        echo json_encode($highscores) . "\n";
        break;
        
    case "submit":
        $game = getGame();
        $data = file_get_contents("php://input");
        if (!$data) $data = file_get_contents("php://stdin");
        $score = new Score($game, $data);
        $rank = DAO::addScore($score);
        echo json_encode($rank) . "\n";
        break; 
        
    default:
        fail(400, "Unknown command");
}
