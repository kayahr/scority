<?php
/*
 * Copyright (C) 2011 Klaus Reimer (k@ailis.de)
 * See LICENSE.txt for licensing information.
 */

// Install class autoloader.
spl_autoload_register(function($className)
{
    $className = preg_replace("/[\\\\_]/", "/", $className);
    return include_once "$className.php";
});

$baseDir = dirname(__FILE__);

// Setup the include path
set_include_path("$baseDir/src" . PATH_SEPARATOR . get_include_path());

// Load configuration
require "config.php";
